'use strict';

var sc = angular.module('stellarClient');

sc.service('HierarchicalAPI', ['StellarNetwork', 'HierarchicalWallet', function(StellarNetwork, HierarchicalWallet) {

    var API = {
        initialize: initialize,
        getAvailableAddress: getAvailableAddress,
        getSufficientPayment: getSufficientPayment,
        getAddresses: getAddresses,
        calculateBalance: calculateBalance,
        ChildAcc: ChildAcc
    };

    var reserveOnWallet = 20000000;
    var remote = undefined;
    var wallet = undefined;
    // array of all the child accounts
    var accounts = [];
    // address to be used as the next anonymous address
    var nextAddress;

    // if account is not ready, functions will be executed later
    var callbacks = [];
    var loaded = false;

    function addCallback(method, args) {
        callbacks.push({
            method: method,
            args: args
        });
    };

    function executeCallbacks() {
        for (var i = 0; i < callbacks.length; i++) {
            API[callbacks[i].method].apply(API, callbacks[i].args);
        };
    };

    function initialize(seed) {
        wallet = new HierarchicalWallet(seed);
        findAccounts();
    }

    function handleAccountLoad() {
        remote = StellarNetwork.remote;
        //testTransaction();
    };

    function testTransaction() {
        var tx = remote.transaction();

        var address = "gKS2REdtftJDgVvrGhMLJeen1KgZVThm8b";
        var secret = "s3m6Txbi3KZj74Xwo9uBnF8Pyz8itbQBru9mgX1Ubxx7FVU5yqx";
        var destination = "gLvvpGwJLMxubi8ag98LNExuca5GvCAMzR";
        var amount = stellar.Amount.from_human("1STR");

        StellarNetwork.remote.set_secret(address, secret);
        var p = {
            from: address,
            to: destination,
            amount: amount.to_json()
        };

        tx.payment(p);

        tx.on('success', function(res) {
            alert("success");
        });
        tx.on('error', function(res) {
            console.log("error");
            console.log(res);
        });

        tx.submit();
    };

    //gets remote object
    StellarNetwork.ensureConnection().then(handleAccountLoad);

    function findAccounts() {
        var seq = 1;
        var address = wallet.getAddress(seq);
        var secret = wallet.getSecret(seq);

        var iterate;
        iterate = function(address) {
            remote.requestAccountInfo(address, function(err, info) {
                // found available
                if (err && err.remote.error_code == '15') {
                    for (var i = 0; i < accounts.length; i++) {
                        console.log(accounts[i].address + " " + accounts[i].amount.to_human_full());
                    };
                    nextAddress = address;
                    loaded = true;
                    executeCallbacks();
                    return;
                }
                // else try next child
                else {
                    var amount = new stellar.Amount.from_human("0STR").add(info.account_data.Balance);
                    accounts.push(new ChildAcc({
                        address: address,
                        secret: secret,
                        amount: amount
                    }));
                    address = wallet.getAddress(++seq);
                    secret = wallet.getSecret(seq);
                    iterate(address);
                }
            });
        };
        iterate(address);
    }

    //returns first available address
    function getAvailableAddress(callback) {
        if (!loaded) {
            addCallback("getAvailableAddress", arguments)
        } else {
            callback(nextAddress);
        }
    };

    //calculates balance without initial address
    function calculateBalance(callback) {
        if (!loaded) {
            addCallback("calculateBalance", arguments)
        } else {
            var entire = stellar.Amount.from_human('0STR');
            var available = stellar.Amount.from_human('0STR');

            for (var i = 0; i < accounts.length; i++) {
                entire = entire.add(accounts[i].amount);
                available = available.add(accounts[i].amount.subtract(reserveOnWallet));
            };
            callback({
                entire: entire.to_number(),
                available: available.to_number()
            });
        }
    };

    //returns an array of ChildAcc objects that will be sufficient for the payment
    //toPay should be passed as an Amount object
    function getSufficientPayment(toPay, callback) {
        if (!toPay) {
            callback(undefined);
        } else if (!loaded) {
            addCallback("getSufficientPayment", arguments);
        } else {


            // will aggregate money from accounts
            var totalAmount = stellar.Amount.from_human('0STR');
            // will have enough accounts to satisfy the payment
            var paymentAccounts = [];

            for (var i = 0;
                (i < accounts.length) && (totalAmount.compareTo(toPay)) < 0; i++) {
                // no money on this account
                if (accounts[i].amount.subtract(reserveOnWallet).is_negative()) {
                    continue;
                }
                //cut payment from the last account to not to overpay
                else if (totalAmount.add(accounts[i].amount.subtract(reserveOnWallet))
                    .compareTo(toPay) >= 0) {

                    var difference = toPay.subtract(totalAmount);
                    totalAmount = totalAmount.add(difference);
                    accounts[i].amount = difference;
                } else {
                    totalAmount = totalAmount.add(accounts[i].amount.subtract(reserveOnWallet));
                    accounts[i].amount = accounts[i].amount.subtract(reserveOnWallet);
                }
                paymentAccounts.push(accounts[i])
            }
            console.log(totalAmount.to_human_full());
            if (totalAmount.compareTo(toPay) >= 0) {
                callback(undefined, paymentAccounts);
            } else {
                callback({
                    'errorDescription': 'notEnoughFunds'
                });
            }
        }
    }

    //constructor of object with info about child account with money
    function ChildAcc(init) {
        this.address = init.address; //string
        this.secret = init.secret; //string
        this.amount = init.amount; //Amount object
    };

    function getAddresses(callback) {
        if (!loaded) {
            addCallback('getAddresses', arguments);
        } else {
            var addresses = [];
            for (var i = 0; i < accounts.length; i++) {
                addresses[i] = accounts[i].address;
            };
            callback(addresses);
        }
    }

    return API;
}]);