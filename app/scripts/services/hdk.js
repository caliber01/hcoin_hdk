'use strict';

var sc = angular.module('stellarClient');

sc.service('HierarchicalAPI', ['StellarNetwork', 'HierarchicalWallet', function(StellarNetwork, HierarchicalWallet) {


    var object = {};
    var OPERATION_CALC_BALANCE = 'balance';
    var OPERATION_GET_ADDRESS = 'address';
    var OPERATION_GET_PAYMENT_ACCOUNTS = 'payments';

    var reserveOnWallet = 20000000;
    var remote;

    var handleAccountLoad = function() {
        remote = StellarNetwork.remote;

        var tx = remote.transaction();
        var address = "g4asdU9oPb1EqLvSeE6PXV7XZk7hiM7nE7";
        var secret = "s3ZE5JTtucnBXtDVz39rjrvxmvsoQFgQN8aWZB9CKmK8655HDBu";
        var destination = "gLvvpGwJLMxubi8ag98LNExuca5GvCAMzR";
        var amount = stellar.Amount.from_human("1STR");

        StellarNetwork.remote.set_secret(address, secret);
        var p = {
            from: address,
            to: destination,
            amount: amount.to_json()
        };

        tx.payment(p);

        tx.on('success', function(res) {
            alert("success");
        });
        tx.on('error', function(res) {
            console.log("error");
            console.log(res);
        });

        tx.submit();
    };

    //gets remote object
    StellarNetwork.ensureConnection().then(handleAccountLoad);

    var toType = function(obj) {
        return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
    }

    var operate = function(privKey, callbackSuccess, operation) {
        var wallet = new HierarchicalWallet(privKey);
        var seq = 1;
        var address = wallet.getAddress(seq);

        var iterate;

        switch (operation) {
            case OPERATION_GET_ADDRESS:
                iterate = function(address) {
                    remote.requestAccountInfo(address, function(err, res) {
                        // found available
                        if (err && err.remote.error_code == '15' || res.account_data.Balance == '0') {
                            callbackSuccess(address);
                            return;
                        }
                        // else try next child
                        else {
                            address = wallet.getAddress(++seq);
                            iterate(address);
                        }
                    });
                };
                iterate(address);
                break;

            case OPERATION_CALC_BALANCE:
                var sum = 0;
                var withoutReserve = 0;
                iterate = function(address) {
                    console.log(address);
                    remote.requestAccountInfo(address, function(err, res) {
                        if ((err && err.remote.error_code == '15') || res.account_data.Balance == '0') {
                            callbackSuccess({
                                entire: sum,
                                available: withoutReserve
                            });
                            return;
                        } else {
                            sum += parseInt(res.account_data.Balance);
                            withoutReserve += parseInt(res.account_data.Balance) - reserveOnWallet;
                            address = wallet.getAddress(++seq);
                            iterate(address);
                        }
                    });
                };
                iterate(address);
                break;

            case OPERATION_GET_PAYMENT_ACCOUNTS:
                var accounts = [];
                iterate = function(address) {
                    remote.requestAccountBalance(address, function(err, res) {
                        //found last funded account
                        if (err && err.remote.error === 'entryNotFound') {
                            callbackSuccess(accounts);
                            return;
                        } else //try next account
                        {
                            var secret = wallet.getSecret(seq);
                            // console.log(address);
                            // console.log(wallet.getAddress(seq))
                            accounts.push(
                                new object.ChildAcc(
                                    address,
                                    secret,
                                    res
                                )
                            );

                            address = wallet.getAddress(++seq);
                            iterate(address);
                        }
                    });
                };
                iterate(address);
                break;
        }
    };


    //convert string to hex
    var toHex = function(str) {
        var hex = str.toString(); //force conversion
        var res = '';
        for (var i = 0; i < hex.length; i++) {
            res += hex.charCodeAt(i).toString(16);
        }
        return res;
    };

    //returns an array of ChildAcc objects with money on them
    var getPaymentAccounts = function(privKey, callback) {
        operate(privKey, callback, OPERATION_GET_PAYMENT_ACCOUNTS);
    };



    //returns first available address
    object.getAvailableAddress = function(privKey, callback) {
        operate(privKey, callback, OPERATION_GET_ADDRESS);
    };

    //calculates balance without initial address
    object.calculateBalance = function(privKey, callback) {
        operate(privKey, callback, OPERATION_CALC_BALANCE);
    };

    //returns an array of ChildAcc objects that will be sufficient for the payment
    //amount should be passed as an Amount object
    object.getSufficientPayment = function(privKey, amount, callback) {
        if (!amount) {
            callback(undefined);
        }
        getPaymentAccounts(privKey, function(res) {
            var totalAmount = stellar.Amount.from_human('0STR');

            var paymentAccounts = [];
            for (var i = 0;
                (i < res.length) && (totalAmount.compareTo(amount)) < 0; i++) {
                //cut payment from the last account to not to overpay
                if (totalAmount.add(res[i].amount).compareTo(amount) >= 0) {
                    amount._is_negative = true;
                    var difference = amount.add(totalAmount.add(res[i].amount));
                    difference._is_negative = true;
                    res[i].amount = res[i].amount.add(difference);
                }

                totalAmount.add(res[i].amount.add(-20000000));

                paymentAccounts.push(res[i]);
            }
            if (totalAmount.compareTo(amount) >= 0) {
                callback(undefined, paymentAccounts);
            } else {
                callback({
                    'errorDescription': 'notEnoughFunds'
                });
            }
        });
    }

    //constructor of object with info about child account with money
    object.ChildAcc = function(address, secret, amount) {
        this.address = address; //string
        this.secret = secret; //string
        this.amount = amount; //Amount object
    };

    return object;
}]);