'use strict';

var sc = angular.module("stellarClient")
sc.service("HierarchicalWallet", [function() {
    var sjcl = ripple.sjcl;
    var base58 = stellar.Base;

    Uint8Array.prototype.concat = function(other) {
        var res = [];
        for (var i = 0; i < this.length; i++) {
            res[i] = this[i];
        };
        for (var i = 0; i < other.length; i++) {
            res[i + this.length] = other[i];
        };
        return new Uint8Array(res);
    };

    Uint8Array.prototype.toBytes = function() {
        var bytes = [];
        for (var i = 0; i < this.length; i++) {
            bytes.push(this[i]);
        };
        return bytes;
    }

    var ed25519 = (function() {
        var that = {};

        var p = "7fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffed";
        var module = sjcl.bn.fromBits(sjcl.codec.hex.toBits(p));

        that.module = module;
        that.moduleBytes = sjcl.codec.bytes.fromBits(module.toBits());

        return that;
    })();

    // Constructor of a wallet
    // -> String
    // <- STRWallet
    function STRWallet(seed) {
        if (!seed) {
            throw "Invalid seed.";
        }

        var generators = generateRootDeterministicKey(seed);

        this.seed = seed;
        this.publicGenerator = generators.publicGenerator;
        this.privateGenerator = generators.privateGenerator;
    }


    // returns pair of secret and public keys as bytes at sequence
    // -> Number
    // <- { Uint8Array, Uint8Array }
    STRWallet.prototype.getKeyPair = function(sequence) {
        var priv = this.getPrivateKey(sequence);
        return nacl.sign.keyPair.fromSeed(priv);
    }

    // returns 32byte private seed as bytes at sequence
    // -> Number
    // <- Uint8Array
    STRWallet.prototype.getPrivateKey = function(sequence) {
        var bytes = this.publicGenerator.concat(new Uint8Array(sequence));
        var seed = firstHalfOfSHA512(bytes);
        return seed;

    };


    // returns corresponding public key at sequence
    // -> Number
    // <- Uint8Array
    STRWallet.prototype.getPublicKey = function(sequence) {
        return this.getKeyPair(sequence).publicKey;
    };

    // returns base58-formatted address
    // -> Number
    // <- String
    STRWallet.prototype.getAddress = function(sequence) {
        sequence = sequence || 0;
        var pub = this.getPublicKey(sequence);
        return formatAddress(pub);
    }

    //  returns base58-formatted secret
    // -> Number
    // <- String
    STRWallet.prototype.getSecret = function(sequence) {
        sequence = sequence || 0;
        var priv = this.getPrivateKey(sequence);
        return formatSecret(priv);
    }

    // returns first 256 bit of sha512
    // -> Uint8Array
    // <- Uint8Array
    function firstHalfOfSHA512(array) {
        var bytes = array.toBytes();
        return new Uint8Array(sjcl.codec.bytes.fromBits(sjcl.bitArray.bitSlice(
            sjcl.hash.sha512.hash(sjcl.codec.bytes.toBits(bytes)),
            0, 256)));
    }

    // function append_int(a, i) {
    //     return [].concat(a, i >> 24, (i >> 16) & 0xff, (i >> 8) & 0xff, i & 0xff)
    // }

    // generate wallet generators from the seed formatted as base58
    // -> String
    // <- { Uint8Array, Uint8Array }
    function generateRootDeterministicKey(seed) {
        var privateGenerator = getPrivateGenerator(seed);
        var publicGenerator = getPublicGenerator(privateGenerator);
        return {
            publicGenerator: publicGenerator,
            privateGenerator: privateGenerator
        };
    };

    // -> String
    // <- Uint8Array
    function getPrivateGenerator(seed) {
        var bytes = base58.decode_check(33, seed).toByteArray();
        var privGen = firstHalfOfSHA512(new Uint8Array(bytes));
        return new Uint8Array(bytes);
    }

    // -> 32byte Uint8Array
    // <- Uint8Array
    function getPublicGenerator(privateGenerator) {
        /* Compute the public generator using from the 
           private generator on the elliptic curve
        */
        return nacl.sign.keyPair.fromSeed(privateGenerator).publicKey;
    }

    // -> sjcl bytes
    // <- String
    function formatAddress(publicKey) {
        /* 
          Encode the EC public key as a stellar address 
          using SHA256 and then RIPEMD160
        */
        var publicKeyBytes = sjcl.codec.bytes
            .fromBits(SHA256_RIPEMD160(sjcl.codec.bytes.toBits(publicKey)));
        return base58.encode_check(0, publicKeyBytes);
    }

    // -> Uint8Array
    // <- String
    function formatSecret(privateKey) {
        var bytes = privateKey.toBytes();
        return base58.encode_check(33, bytes);
    };


    function SHA256_RIPEMD160(bits) {
        return sjcl.hash.ripemd160.hash(sjcl.hash.sha256.hash(bits));
    }

    // testing vector
    (function test() {
        var wallet = new STRWallet('sfGHndzNLXq3NF1feHutjEmLpMN7F6hHkRPDcSixBP16dauy3ju');
        var sec = wallet.getSecret(1);
        var add = wallet.getAddress(1);
        console.log(add);
        console.log(sec);
    });

    return STRWallet;
}]);