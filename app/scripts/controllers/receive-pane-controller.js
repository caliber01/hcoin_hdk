'use strict';

var sc = angular.module('stellarClient');

sc.controller('ReceivePaneCtrl', function($scope, session) {
    $scope.showAddress = false;

    $scope.currentAddress = function() {
        return session.get('address');
    };

    $scope.generatedAddress = function() {
        return session.get('generatedAddress');
    }

    $scope.toggleAddress = function() {
        $scope.showAddress = !$scope.showAddress;
    };
});